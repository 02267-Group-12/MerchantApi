package dk.group12;

import java.io.Serializable;
import java.util.UUID;

import javax.ws.rs.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.*;

@Data
public class Merchant implements Serializable{
    public Merchant() {}

    public Merchant(UUID accId, String accountId, String bankAccountId, String accountType) {
        this.accountUUID = accId;
        this.accountId = accountId;
        this.bankAccountId = bankAccountId;
        this.accountType = accountType;
  }
    public String accountId = null;
    public String bankAccountId = null;

    @JsonIgnore
    public String accountType = "merchant";
    @JsonIgnore
    public UUID accountUUID = null;
    
}