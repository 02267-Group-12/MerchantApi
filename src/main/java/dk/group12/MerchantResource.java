package dk.group12;

import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.jboss.logging.Logger;

import io.smallrye.common.annotation.NonBlocking;
import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.core.MediaType;

@Path("/merchant")
public class MerchantResource {

    private static final Logger LOG = Logger.getLogger(MerchantResource.class);

    @Inject
    MerchantService mService;

    @GET
    @NonBlocking
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{merchantId}")
    public Uni<Merchant> get(@PathParam(value = "merchantId") UUID merchantId) {
        return mService.retrieveMerchant(merchantId);
    }

    @POST
    @NonBlocking
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<Merchant> merchantRegistrationRequest(final Merchant merchant) { // Post
        return mService.registerMerchant(merchant).log("Final Output");
    }

    @POST
    @NonBlocking
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{merchantId}/payment")
    public Uni<String> createPayment(@PathParam(value = "merchantId") UUID merchantId, Payment paymentRequest) {
        return mService.createPayment(paymentRequest, merchantId);
    }

    @GET
    @NonBlocking
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{merchantId}/report")
    public Uni<String[][]> getReport(@PathParam(value = "merchantId") UUID merchantId) {
        return mService.getReport(merchantId);
    }

    // @GET
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // @Path("/{merchantId}")
    // public Merchant get(@PathParam(value = "merchantId") UUID merchantId) {
    // LOG.infof("Request for account with ID: %s", merchantId);
    // CorrelationId correlationId = CorrelationId.randomId();
    // LOG.infof("CorrelationId: \t %s", correlationId.getId());
    // Event ev = new Event("request-retrieve-account", new Object[] {merchantId,
    // correlationId});
    // JsonObject asJsonObj = new JsonObject(new Gson().toJson(ev));
    // LOG.info(asJsonObj);
    // requestRetrieveAccountQueue.send(asJsonObj);

    // Merchant merchant = null;
    // Iterable<JsonObject> iterable =
    // accountRetrievedQueue.subscribe().asIterable();
    // for (JsonObject item : iterable) {
    // LOG.infof("Received Account-Retrieved event: %s", item.toString());
    // Event inEvent = new Gson().fromJson(item.toString(), Event.class);
    // CorrelationId returnedCorrelationId = inEvent.getArgument(1,
    // CorrelationId.class);
    // if (correlationId.getId().equals(returnedCorrelationId.getId())) {
    // LOG.info("Matching CorrelationId found.");
    // merchant = inEvent.getArgument(0, Merchant.class);
    // break;
    // }
    // }
    // return merchant;

    // }

    // @POST
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public Merchant merchantRegistrationRequest(Merchant merchant){
    // LOG.debugf("Received custom registration request with: %s", merchant);
    // CorrelationId correlationId = CorrelationId.randomId();
    // LOG.infof("CorrelationId: \t %s", correlationId.getId());
    // Event ev = new Event("request-account-registration", new Object[] {merchant,
    // correlationId});
    // JsonObject asJsonObj = new JsonObject(new Gson().toJson(ev));
    // LOG.info(asJsonObj);
    // requestAccountRegistrationQueue.send(asJsonObj);

    // UUID returnedId = null;
    // Iterable<JsonObject> iterable =
    // accountRegisteredQueue.subscribe().asIterable();
    // for (JsonObject item : iterable) {
    // LOG.infof("Received Account-Registered event: %s", item.toString());
    // Event inEvent = new Gson().fromJson(item.toString(), Event.class);
    // CorrelationId returnedCorrelationId = inEvent.getArgument(1,
    // CorrelationId.class);
    // if (correlationId.getId().equals(returnedCorrelationId.getId())) {
    // LOG.info("Matching CorrelationId found.");
    // returnedId = inEvent.getArgument(0, UUID.class);
    // merchant.setAccountUUID(returnedId);
    // break;
    // }
    // }
    // return merchant;
    // }

    // @DELETE
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public String merchantDeregistrationRequest(String mId){
    // return mService.merchantDeregistrationRequest(mId);
    // }

    // @Path("/pay")
    // @POST
    // @Consumes(MediaType.APPLICATION_JSON)
    // @Produces(MediaType.APPLICATION_JSON)
    // public String paymentRequest(final Payment payment){
    // return mService.paymentRequest(payment);
    // }
}
