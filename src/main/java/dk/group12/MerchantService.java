package dk.group12;

import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.jboss.logging.Logger;

import com.google.gson.Gson;

import dk.group12.events.CorrelationId;
import dk.group12.events.Event;
import io.smallrye.mutiny.Uni;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;
import io.vertx.core.json.JsonObject;

@Singleton
public class MerchantService {

    private static final Logger LOG = Logger.getLogger(MerchantService.class);

    // OUTGOING QUEUES
    @Inject
    @Channel("request-account-registration")
    Emitter<JsonObject> requestAccountRegistrationQueue;

    @Inject
    @Channel("request-retrieve-account")
    Emitter<JsonObject> requestRetrieveAccount;

    @Inject
    @Channel("request-payment")
    Emitter<JsonObject> requestPaymentQueue;

    @Inject
    @Channel("request-report")
    Emitter<JsonObject> requestReportQueue;

    // CorrelationId Maps
    Map<UUID, Uni<UUID>> registeredAccountUUIDMap = new ConcurrentHashMap<>();
    Map<UUID, Uni<Merchant>> retrievedAccountMap = new ConcurrentHashMap<>();
    Map<UUID, Uni<String>> paymentCompletedMap = new ConcurrentHashMap<>();
    Map<UUID, Uni<String[][]>> reportMap = new ConcurrentHashMap<>();
    
    @Incoming("report-completed")
    public void reportRequestCompleted(JsonObject jObj){
        Event ev = convertJsonToObject(jObj, Event.class);

        String[][] report = ev.getArgument(0, String[][].class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);

        reportMap.computeIfPresent(corrId.getId(), (key, val) -> {
            return val.onItem().transformToUni(a -> Uni.createFrom().item(report));
        });
    }

    public Uni<String[][]> getReport(UUID merchantUUID){
        CorrelationId correlationId = CorrelationId.randomId();
        reportMap.put(correlationId.getId(), Uni.createFrom().nullItem());
        Event ev = new Event("request-report", new Object[] { merchantUUID, correlationId });
        JsonObject asJsonObj = convertObjectToJson(ev);
        
        OutgoingRabbitMQMetadata meta = OutgoingRabbitMQMetadata.builder()
                .withReplyTo("merchant")
                .build();

        requestReportQueue.send(Message.of(asJsonObj, Metadata.of(meta)));

        Uni<String[][]> reportUni = reportMap.get(correlationId.getId())
                .onItem().transformToUni(customer -> reportMap.get(correlationId.getId()))
                .onItem().invoke(reports -> LOG.infof("reports gotten:%s", Arrays.toString(reports)))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .onItem().call(() -> reportMap.remove(correlationId.getId()));
        
        return reportUni;
    }

    @Incoming("account-registered")
    public void accountRegistered(JsonObject jObj) {
        LOG.infof("Received event: %s", jObj);
        LOG.debugf("registeredAccountUUIDMap: %s", registeredAccountUUIDMap.toString());
        Event ev = convertJsonToObject(jObj, Event.class);
        UUID accId = ev.getArgument(0, UUID.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
        registeredAccountUUIDMap.computeIfPresent(corrId.getId(), (key, val) -> {
            LOG.infof("Setting the value AccId to: %s for corrId %s", accId, corrId.getId());
            return val.onItem().transformToUni(a -> Uni.createFrom().item(accId));
        });
    }

    @Incoming("account-retrieved")
    public void accountRetrieved(JsonObject jObj) {
        LOG.infof("Received event: %s", jObj);
        LOG.debugf("registeredAccountUUIDMap: %s", retrievedAccountMap.toString());
        Event ev = convertJsonToObject(jObj, Event.class);
        Merchant merchant = ev.getArgument(0, Merchant.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
        retrievedAccountMap.computeIfPresent(corrId.getId(), (key, val) -> {
            LOG.infof("Setting the value Acc to: %s for corrId %s", merchant, corrId.getId());
            return val.onItem().transformToUni(c -> Uni.createFrom().item(merchant));
        });
    }

    @Incoming("payment-completed")
    public void paymentCompleted(JsonObject jObj) {
        LOG.infof("Received event: %s", jObj);
        LOG.debugf("paymentCompleteMap: %s", paymentCompletedMap.toString());
        Event ev = convertJsonToObject(jObj, Event.class);
        String returnedMessage = ev.getArgument(0, String.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
        paymentCompletedMap.computeIfPresent(corrId.getId(), (key, val) -> {
            LOG.infof("Setting the value payment to: %s for corrId %s", returnedMessage, corrId.getId());
            return val.onItem().transformToUni(c -> Uni.createFrom().item(returnedMessage));
        });
    }

    public Uni<Merchant> retrieveMerchant(UUID merchantId) {
        LOG.infof("Request for account with ID: %s", merchantId);
        CorrelationId correlationId = CorrelationId.randomId();
        LOG.infof("CorrelationId: \t %s", correlationId.getId());
        retrievedAccountMap.put(correlationId.getId(), Uni.createFrom().nullItem());
        Event ev = new Event("request-retrieve-account", new Object[] { merchantId, correlationId });
        JsonObject asJsonObj = convertObjectToJson(ev);
        LOG.info(asJsonObj);

        OutgoingRabbitMQMetadata meta = OutgoingRabbitMQMetadata.builder()
                .withReplyTo("merchant")
                .build();

        requestRetrieveAccount.send(Message.of(asJsonObj, Metadata.of(meta)));

        LOG.infof("Current Retrieved Accounts waiting: %s", registeredAccountUUIDMap.toString());
        Uni<Merchant> merchantUni = retrievedAccountMap.get(correlationId.getId())
                .onItem().transformToUni(customer -> retrievedAccountMap.get(correlationId.getId()))
                .onItem().invoke(c -> LOG.infof("retrieved acc of:%s", c))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .onItem().call(() -> retrievedAccountMap.remove(correlationId.getId()));
        return merchantUni;
    }

    public Uni<Merchant> registerMerchant(Merchant merchant) {
        LOG.debugf("Received custom registration request with: %s", merchant);
        CorrelationId correlationId = CorrelationId.randomId();
        LOG.infof("CorrelationId: \t %s", correlationId.getId());
        registeredAccountUUIDMap.put(correlationId.getId(), Uni.createFrom().nullItem());
        Event ev = new Event("request-account-registration", new Object[] { merchant, correlationId });
        JsonObject asJsonObj = convertObjectToJson(ev);
        LOG.info(asJsonObj);

        OutgoingRabbitMQMetadata meta = OutgoingRabbitMQMetadata.builder()
                .withReplyTo("merchant")
                .build();

        requestAccountRegistrationQueue.send(Message.of(asJsonObj, Metadata.of(meta)));

        LOG.infof("Current Registered Accounts waiting: ", registeredAccountUUIDMap.toString());
        Uni<Merchant> merchantUni = registeredAccountUUIDMap.get(correlationId.getId())
                .onItem().transformToUni(id -> registeredAccountUUIDMap.get(correlationId.getId()))
                .onItem().invoke(i -> LOG.infof("retrieved accId of:%s", i))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                // Deal with Success : Map to Customer
                .map(accId -> new Merchant(
                        accId,
                        merchant.getAccountId(),
                        merchant.getBankAccountId(),
                        merchant.getAccountType()))
                // Clean up Map
                .onItem().call(() -> registeredAccountUUIDMap.remove(correlationId.getId()));

        return merchantUni;
    }

    public Uni<String> createPayment(Payment payment, UUID merchantId) {
        payment.setCreditorId(merchantId);
        LOG.debugf("Received payment request with: %s for merchant %s", payment, merchantId);
        CorrelationId correlationId = CorrelationId.randomId();
        LOG.infof("CorrelationId: \t %s", correlationId.getId());
        paymentCompletedMap.put(correlationId.getId(), Uni.createFrom().nullItem());
        Event ev = new Event("request-payment", new Object[] {payment, correlationId});
        JsonObject asJsonObj = new JsonObject(new Gson().toJson(ev));
        LOG.info(asJsonObj);
        requestPaymentQueue.send(asJsonObj);


        LOG.infof("Current Registered Accounts waiting: ", registeredAccountUUIDMap.toString());
        Uni<String> paymentUni = paymentCompletedMap.get(correlationId.getId())
        .onItem().transformToUni(id -> paymentCompletedMap.get(correlationId.getId()))
        .onItem().invoke(i -> LOG.infof("retrieved returned statement of:%s",i))
        // Check for failure
        .onItem().ifNull().fail()
        // Deal with Failure
        .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
        // .onFailure().recoverWithItem("Failed for some reason.")
        // Clean up Map
        .onTermination().invoke(() -> paymentCompletedMap.remove(correlationId.getId()));
        
        // registeredAccountUUIDMap.remove(correlationId.getId());
       return paymentUni;


    }

    private <T> T convertJsonToObject(JsonObject jObj, Class<T> classType) {
        return new Gson().fromJson(jObj.toString(), classType);
    }

    private JsonObject convertObjectToJson(Object obj) {
        return new JsonObject(new Gson().toJson(obj, obj.getClass()));
    }

}