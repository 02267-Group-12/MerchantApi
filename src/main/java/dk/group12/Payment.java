package dk.group12;

import lombok.*;
import java.util.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.math.BigDecimal;

@Data
public class Payment{
    public UUID debtorToken;
    public BigDecimal amount;
    public String description;

    @JsonIgnore
    public UUID creditorId;
}